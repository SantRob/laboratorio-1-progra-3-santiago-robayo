﻿namespace activity1Sol;

class Program
{
    static void Main(string[] args)
    {
        int count = 1;
        if (args.Length != 0)
        {
            foreach(var arg in args)
            {
                Console.WriteLine($"Argument number {count} is: {arg}");
                count++;
            }
        } else 
        {
            Console.WriteLine("You did not enter any argument.");
        }
        Console.WriteLine();
        Conversions();
    }

    static short IntToShort (int num)
    {
        return (short)num;
    }

    static long IntToLong (int num)
    {
        return (long)num;
    }

    static float IntToFloat (int num)
    {
        return (float)num;
    }

    static double IntToDouble (int num)
    {
        return (double)num;
    }

    static string IntToString (int num)
    {
        return num.ToString();
    }

    static int FloatToInt (float num)
    {
        return (int)num;
    }

    static string BoolToString (bool value)
    {
        return value.ToString();
    }

    static object Boxing (int num)
    {
        object intObj = num;
        return intObj;
    }

    static int Unboxing (object intObj)
    {
        return (int)intObj;
    }

    static void Conversions()
    {
        int intValue = 42;
        float floatValue = 3.14f;
        bool boolValue = true;

        Console.WriteLine($"int a short: {intValue} = {IntToShort(intValue)}");

        Console.WriteLine($"int a long: {intValue} = {IntToLong(intValue)}");

        Console.WriteLine($"float a int: {floatValue} = {FloatToInt(floatValue)}");

        Console.WriteLine($"int a float: {intValue} = {IntToFloat(intValue)}");

        Console.WriteLine($"int a double: {intValue} = {IntToDouble(intValue)}");

        Console.WriteLine($"bool a string: {boolValue} = {BoolToString(boolValue)}");

        Console.WriteLine($"int a string: {intValue} = {IntToString(intValue)}");

        object boxedInt = Boxing(intValue);
        Console.WriteLine($"Boxing (int a object): {intValue} = {boxedInt}");

        int unboxedInt = Unboxing(boxedInt);
        Console.WriteLine($"Unboxing (object a int): {boxedInt} = {unboxedInt}");
    }
}
